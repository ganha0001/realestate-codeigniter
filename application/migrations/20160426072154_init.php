<?php
/**
 * Migration: init
 *
 * Created by: Cli for CodeIgniter <https://github.com/kenjis/codeigniter-cli>
 * Created on: 2016/04/26 07:21:54
 */
class Migration_init extends CI_Migration {

	public function up()
	{
//		// Creating a table
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE,
				'unsigned'=> TRUE
			),
			'profile_id' => array(
				'type' => 'INT',
				'unsigned'=> TRUE,
				'constraint' => 11,
				'NULL' => TRUE,
			),
			'plan_id' => array(
				'type' =>'int',
				'unsigned'=> TRUE,
				'constraint' => '11',
				'default' => 0,
			),
			'role' => array(
				'type' => 'TINYINT',
				'unsigned'=> TRUE,
				'default' => 0,
			),
			'status' => array(
				'type' => 'BOOLEAN',
				'default' => 0,
			),
			'username' => array(
				'type' => 'VARCHAR',
				'constraint' => 30,
			),
			'password_hash' => array(
				'type' => 'VARCHAR',
				'constraint' => 30,
			),
			'password_reset_token' => array(
				'type' => 'VARCHAR',
				'constraint' => 16,
				'NULL' => TRUE,
			),
			'avatar' => array(
				'type' => 'VARCHAR',
				'constraint' => 500,
				'NULL' => TRUE,
			),
			'full_name' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'NULL' => TRUE,
			),
			'phone_number' => array(
				'type' => 'VARCHAR',
				'constraint' => 15,
				'NULL' => TRUE,
			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('users');

		// rooms

		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE,
				'unsigned'=> TRUE
			),
			'user_id' => array(
				'type' => 'INT',
				'unsigned'=> TRUE,
				'constraint' => 11,
			),
			'area' => array(
				'type' =>'FLOAT',
				'unsigned'=> TRUE,
			),
			'location' => array(
				'type' => 'TEXT',
				'NULL' => TRUE
			),
			'longitude' => array(
				'type' =>'FLOAT',
				'unsigned'=> TRUE,
			),
			'latitude' => array(
				'type' =>'FLOAT',
				'unsigned'=> TRUE,
			),
			'rest_room' => array(
				'type' => 'BOOLEAN',
			),
			'description' => array(
				'type' => 'TEXT',
				'NULL' => TRUE,
			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('rooms');

		// apartment

		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE,
				'unsigned'=> TRUE
			),
			'user_id' => array(
				'type' => 'INT',
				'unsigned'=> TRUE,
				'constraint' => 11,
			),
			'area' => array(
				'type' =>'FLOAT',
				'unsigned'=> TRUE,
			),
			'location' => array(
				'type' => 'TEXT',
				'NULL' => TRUE
			),
			'longitude' => array(
				'type' =>'FLOAT',
				'unsigned'=> TRUE,
			),
			'latitude' => array(
				'type' =>'FLOAT',
				'unsigned'=> TRUE,
			),
			'no_rest_room' => array(
				'type' => 'TINYINT',
				'unsigned'=> TRUE,
			),
			'no_room' => array(
				'type' => 'TINYINT',
				'unsigned'=> TRUE,
			),
			'description' => array(
				'type' => 'TEXT',
				'NULL' => TRUE,
			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('apartments');

		// houses

		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE,
				'unsigned'=> TRUE
			),
			'user_id' => array(
				'type' => 'INT',
				'unsigned'=> TRUE,
				'constraint' => 11,
			),
			'area' => array(
				'type' =>'FLOAT',
				'unsigned'=> TRUE,
			),
			'location' => array(
				'type' => 'TEXT',
				'NULL' => TRUE
			),
			'longitude' => array(
				'type' =>'FLOAT',
				'unsigned'=> TRUE,
			),
			'latitude' => array(
				'type' =>'FLOAT',
				'unsigned'=> TRUE,
			),
			'no_rest_room' => array(
				'type' => 'TINYINT',
				'unsigned'=> TRUE,
			),
			'no_room' => array(
				'type' => 'TINYINT',
				'unsigned'=> TRUE,
			),
			'description' => array(
				'type' => 'TEXT',
				'NULL' => TRUE,
			),
			'created_at' => array(
				'type' => 'TIMESTAMP',
			),
			'updated_at' => array(
				'type' => 'TIMESTAMP',
			),
		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('houses');

//		// Adding a Column to a Table
//		$fields = array(
//			'preferences' => array('type' => 'TEXT')
//		);
//		$this->dbforge->add_column('table_name', $fields);
	}

	public function down()
	{
//		// Dropping a table
		$this->dbforge->drop_table('users');
		$this->dbforge->drop_table('apartments');
		$this->dbforge->drop_table('houses');
//		// Dropping a Column From a Table
//		$this->dbforge->drop_column('table_name', 'column_to_drop');
	}

}
