<?php 
defined('BASEPATH') or exit('No direct script access allowed');

Class RoomsModel extends MY_Model {

	public $table = 'rooms'; // Set the name of the table for this model.
    public $primary_key = 'id'; // Set the primary key


	public function __construct() {
		$this->has_one['users'] = array('UsersModel','user_id','id');
		$this->timestamps_format = 'timestamp';
		parent:: __construct();
	}
}