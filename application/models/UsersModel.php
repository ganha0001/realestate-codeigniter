<?php 
defined('BASEPATH') or exit('No direct script access allowed');

Class UsersModel extends MY_Model {

	public $table = 'users'; // Set the name of the table for this model.
    public $primary_key = 'id'; // Set the primary key


	public function __construct() {
		$this->timestamps_format = 'timestamp';
		$this->has_many['rooms'] = array('RoomsModel','id','user_id');

		parent:: __construct();
	}
}