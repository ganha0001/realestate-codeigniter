<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class getJSONController extends MY_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function users ($username = NULL) {
		$this->load->model('UsersModel', 'users');
		if ($username == NULL) echo json_encode($this->users->get_all());
			else echo json_encode($this->users->where('username',$username)->get());
	}

	public function rooms ($id = NULL) {
		$this->load->model('RoomsModel', 'rooms');
		if ($id == NULL) echo json_encode($this->rooms->get_all());
			else echo json_encode($this->rooms->where('id', $id)->get());
	}
}
