<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class posthandlerController extends MY_Controller {

	public function __construct(){
		parent::__construct();
	}

	public function post()
	{
		if ($this->input->post()){
			echo json_encode($this->input->post());
		}
	}

}
