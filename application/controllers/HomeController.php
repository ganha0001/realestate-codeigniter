<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class HomeController extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('UsersModel', 'users');
	}

	public function index()
	{
		$data = [
			'token' => $this->security->get_csrf_hash(),
		];
		$this->blade->render("login", $data);
	}

	public function btnSubmit(){
		
	}
}
